### TCP

https://www.rfc-editor.org/rfc/rfc793#3.1 (Obsoleted :())

---

The TCP must recover from data that is damaged, lost, duplicated, or
    delivered out of order by the internet communication system.  This
    is achieved by assigning a sequence number to each octet
    transmitted, and requiring a positive acknowledgment (ACK) from the
    receiving TCP.  If the ACK is not received within a timeout
    interval, the data is retransmitted.  At the receiver, the sequence
    numbers are used to correctly order segments that may be received
    out of order and to eliminate duplicates.  Damage is handled by
    adding a checksum to each segment transmitted, checking it at the
    receiver, and discarding damaged segments.

---

TCP provides a means for the receiver to govern the amount of data
    sent by the sender.  This is achieved by returning a "window" with
    every ACK indicating a range of acceptable sequence numbers beyond
    the last segment successfully received.  The window indicates an
    allowed number of octets that the sender may transmit before
    receiving further permission.

---

To allow for many processes within a single Host to use TCP
    communication facilities simultaneously, the TCP provides a set of
    addresses or ports within each host.  Concatenated with the network
    and host addresses from the internet communication layer, this forms
    a socket.  A pair of sockets uniquely identifies each connection.
    That is, a socket may be simultaneously used in multiple
    connections.

---

The combination of this information, including
    sockets, sequence numbers, and window sizes, is called a connection.
    Each connection is uniquely specified by a pair of sockets
    identifying its two sides.

---

Since connections must be established between unreliable hosts and
    over the unreliable internet communication system, a handshake
    mechanism with clock-based sequence numbers is used to avoid
    erroneous initialization of connections

* what's handshake

---

 Processes transmit data by calling on the TCP and passing buffers of
  data as arguments.  The TCP packages the data from these buffers into
  segments and calls on the internet module to transmit each segment to
  the destination TCP.  The receiving TCP places the data from a segment
  into the receiving user's buffer and notifies the receiving user.  The
  TCPs include control information in the segments which they use to
  ensure reliable ordered data transmission.

---

 The TCP is assumed to be a module in an operating system.
 The actual interface to the network is assumed to be
  controlled by a device driver module. 

---

When the TCP transmits a
  segment containing data, it puts a copy on a retransmission queue and
  starts a timer; when the acknowledgment for that data is received, the
  segment is deleted from the queue.  If the acknowledgment is not
  received before the timer runs out, the segment is retransmitted.

---

A connection can be used to carry data in both directions,
  that is, it is "full duplex".

---

The OPEN call also specifies
  whether the connection establishment is to be actively pursued, or to
  be passively waited for.

---

Two processes which issue active OPENs to each
  other at the same time will be correctly connected.

---

There are several things that must be remembered
  about a connection.  To store this information we imagine that there
  is a data structure called a Transmission Control Block (TCB). 

---

  The procedures to establish connections utilize the synchronize (SYN)
  control flag and involves an exchange of three messages.  This
  exchange has been termed a three-way hand shake [3].

  A connection is initiated by the rendezvous of an arriving segment
  containing a SYN and a waiting TCB entry each created by a user OPEN
  command.  The matching of local and foreign sockets determines when a
  connection has been initiated.  The connection becomes "established"
  when sequence numbers have been synchronized in both directions.

  The clearing of a connection also involves the exchange of segments,
  in this case carrying the FIN control flag.

---

Each time a PUSH flag is
  associated with data placed into the receiving user's buffer, the
  buffer is returned to the user for processing even if the buffer is
  not filled.  If data arrives that fills the user's buffer before a
  PUSH is seen, the data is passed to the user in buffer size units.

---

be
  conservative in what you do, be liberal in what you accept from
  others.

------


https://www.rfc-editor.org/rfc/rfc9293#3.4


----

Data flow is supported bidirectionally over TCP connections, though applications are free to send data only unidirectionally, if they so choose.

---

The TCP checksum is never optional. The sender MUST generate it (MUST-2) and the receiver MUST check it (MUST-3).

---

A TCP Option, in the mandatory option set, is one of an End of Option List Option, a No-Operation Option, or a Maximum Segment Size Option.

---

The maintenance of a TCP connection requires maintaining state for several variables. We conceive of these variables being stored in a connection record called a Transmission Control Block or TCB. 

---

The sequence number is the byte number of the first byte of data in the TCP packet sent (also called a TCP segment).
The acknowledgement number is the sequence number of the next byte the receiver expects to receive.

---

A connection progresses through a series of states during its lifetime.
The states are: LISTEN, SYN-SENT, SYN-RECEIVED, ESTABLISHED, FIN-WAIT-1, FIN-WAIT-2, CLOSE-WAIT, CLOSING, LAST-ACK, TIME-WAIT,
and the fictional state CLOSED. CLOSED is fictional because it represents the state when there is no TCB, and therefore, no connection.

---

A TCP connection progresses from one state to another in response to events.
The events are the user calls, OPEN, SEND, RECEIVE, CLOSE, ABORT, and STATUS;
the incoming segments, particularly those containing the SYN, ACK, RST, and FIN flags; and timeouts.

---

A fundamental notion in the design is that every octet of data sent over a TCP connection has a sequence number. 

---

It is essential to remember that the actual sequence number space is finite, though large. This space ranges from 0 to 232 - 1.
Since the space is finite, all arithmetic dealing with sequence numbers must be performed modulo 232

---

The following comparisons are needed to process the acknowledgments:

SND.UNA = oldest unacknowledged sequence number

SND.NXT = next sequence number to be sent

SEG.ACK = acknowledgment from the receiving TCP peer (next sequence number expected by the receiving TCP peer)

SEG.SEQ = first sequence number of a segment

SEG.LEN = the number of octets occupied by the data in the segment (counting SYN and FIN)

SEG.SEQ+SEG.LEN-1 = last sequence number of a segment

A new acknowledgment (called an "acceptable ack") is one for which the inequality below holds:

SND.UNA < SEG.ACK =< SND.NXT

---

A segment on the retransmission queue is fully acknowledged if the sum of its sequence number and length is less than or equal to the acknowledgment value in the incoming segment.

---

When data is received, the following comparisons are needed:

RCV.NXT = next sequence number expected on an incoming segment, and is the left or lower edge of the receive window

RCV.NXT+RCV.WND-1 = last sequence number expected on an incoming segment, and is the right or upper edge of the receive window

SEG.SEQ = first sequence number occupied by the incoming segment

SEG.SEQ+SEG.LEN-1 = last sequence number occupied by the incoming segment

A segment is judged to occupy a portion of valid receive sequence space if

RCV.NXT =< SEG.SEQ < RCV.NXT+RCV.WND

or

RCV.NXT =< SEG.SEQ+SEG.LEN-1 < RCV.NXT+RCV.WND

----

Note that when the receive window is zero no segments should be acceptable except ACK segments.

---

ISN = M + F(localip, localport, remoteip, remoteport, secretkey)

where M is the 4 microsecond timer, and F() is a pseudorandom function (PRF) of the connection's identifying parameters ("localip, localport, remoteip, remoteport") and a secret key ("secretkey") (SHLD-1).

---

 1) A --> B  SYN my sequence number is X
    2) A <-- B  ACK your sequence number is X
    3) A <-- B  SYN my sequence number is Y
    4) A --> B  ACK your sequence number is Y
Because steps 2 and 3 can be combined in a single message this is called the three-way (or three message) handshake (3WHS).

---

