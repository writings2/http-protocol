from dataclasses import dataclass
from enum import Enum
from typing import Callable, List


# https://www.rfc-editor.org/rfc/rfc793#section-3.1
# https://realpython.com/python-sockets/
# https://inc0x0.com/tcp-ip-packets-introduction/

def ipv4_to_hex(ip: str) -> List[str]:
    ips = ip.split('.')
    hexed_ip = []
    current_hexed_ip = '0x'
    for idx, ip_part in enumerate(ips, start=1):
        hex_ip_part = f"{int(ip_part):02x}"
        current_hexed_ip += hex_ip_part
        if idx % 2 == 0:
            hexed_ip.append(current_hexed_ip)
            current_hexed_ip = '0x'
    return hexed_ip

def split_to_16bits(value: str) -> List[str]:
    split_values = value.split()
    bits = []
    current_hexed_value = '0x'
    for idx, el in enumerate(split_values, start=1):
        hex_el = f"{int(el):02x}"
        current_hexed_value += hex_el
        if idx % 2 == 0:
            bits.append(current_hexed_value)
            current_hexed_value = '0x'
    return bits

def binary_to_hex(binary: str) -> str:
    num = int(binary, 2)
    return f"0x{num:04x}"

@dataclass
class PseudoHeader:
    protocol: int
    source_address: str
    destination_address: str
    length: int

    def sum(self) -> str:
        protocol_hex = f"0x{self.protocol:04x}"
        source_address_hex = ipv4_to_hex(self.source_address)
        destination_address_hex = ipv4_to_hex(self.destination_address)
        length_hex = f"0x{self.length:04x}"

        sum_hex = 0
        sum_hex += int(protocol_hex, 16)
        for ip_part in source_address_hex:
            sum_hex += int(ip_part, 16)
        for ip_part in destination_address_hex:
            sum_hex += int(ip_part, 16)
        sum_hex += int(length_hex, 16)
        return hex(sum_hex)


@dataclass
class Datagram:
    source_port: int #16bit
    destination_port: int #16bit
    sequence_number: int #32bit
    acknowledgement_number: int #32bit
    data_offset: str #4bit
    reserved: str #4bit
    flags: str #8bit | CWR, ECE, URG, ACK, PSH, RST, SYN, and FIN
    window_size: int #16bit
    checksum: int #16bit
    urgent_pointer: int #16bit
    options: int # size(Options) == (DOffset-5)*32; present only when DOffset > 5
    data: str # size(Data) == TotalLength - HeaderLength

    def count_checksum(self, pseudo_header: PseudoHeader) -> str:
        pseudo_header_checksum = pseudo_header.sum()
        source_port_hex = f"0x{self.source_port:04x}"
        destination_port_hex = f"0x{self.destination_port:04x}"
        sequence_number_hex = split_to_16bits(str(self.sequence_number))
        acknowledgement_number_hex = split_to_16bits(str(self.acknowledgement_number))
        binary_hex = binary_to_hex(self.data_offset + self.reserved + self.flags)
        window_size_hex = f"0x{self.window_size:04x}"
        urgent_pointer_hex = f"0x{self.urgent_pointer:04x}"

        sum_hex = 0
        sum_hex += int(pseudo_header_checksum, 16)
        sum_hex += int(source_port_hex, 16)
        sum_hex += int(destination_port_hex, 16)
        for seq_part in sequence_number_hex:
            sum_hex += int(seq_part, 16)
        for ack_part in acknowledgement_number_hex:
            sum_hex += int(ack_part, 16)
        sum_hex += int(binary_hex, 16)
        sum_hex += int(window_size_hex, 16)
        sum_hex += int(urgent_pointer_hex, 16)
        return hex(sum_hex)


@dataclass
class TransmissionControlBlock:
    remote_ip_address: str
    remote_port_number: int
    local_ip_address: str
    local_port_number: int
    ip_security_level: int
    compartment: int
    send_buffer: str
    receive_buffer: str
    retransmit_queue: str
    current_segment: str
    send_sequence_number: int
    receive_sequence_number: int

class ConnectionStatus(Enum):
    LISTEN = 'LISTEN'
    SYN_SENT = 'SYN_SENT'
    SYN_RECEIVED = 'SYN_RECEIVED'
    ESTABLISHED = 'ESTABLISHED'
    FIN_WAIT_1 = 'FIN_WAIT_1'
    FIN_WAIT_2 = 'FIN_WAIT_2'
    CLOSE_WAIT = 'CLOSE_WAIT'
    CLOSING = 'CLOSING'
    LAST_ACK = 'LAST_ACK'
    TIME_WAIT = 'TIME_WAIT'
    CLOSED = 'CLOSED'

class Events(Enum):
    OPEN = 'OPEN'
    SEND = 'SEND'
    RECEIVE = 'RECEIVE'
    CLOSE = 'CLOSE'
    ABORT = 'ABORT'
    STATUS = 'STATUS'

class Flags(Enum):
    SYN = 'SYN'
    ACK = 'ACK'
    FIN = 'FIN'
    RST = 'RST'

@dataclass
class Transition:
    input: Events|Flags
    next_state: ConnectionStatus
    actions: List[Callable]

STATE_MACHINE_MAP = {
    ConnectionStatus.CLOSED: [
        Transition(Events.OPEN, next_state=ConnectionStatus.SYN_SENT, [is_active, create_tcb, send(FLAGS.SYN)]),
        Transition(Events.OPEN, next_state=ConnectionStatus.LISTEN, [is_passive, create_tcb]),
        # Transition(Flags.RST, next_state=ConnectionStatus.CLOSED, []), # Note 3
    ],
    ConnectionStatus.SYN_SENT: [
        Transition(Events.CLOSE, next_state=ConnectionStatus.CLOSED, [delete_tcb()]),
        Transition(Flags.SYN, next_state=ConnectionStatus.SYN_RECEIVED, [send(FLAGS.SYN), send(FLAGS.ACK)]),
        Transition([Flags.SYN, Flags.ACK], next_state=ConnectionStatus.ESTABLISHED, [send(FLAGS.ACK)]),
        # Transition(Flags.RST, next_state=ConnectionStatus.CLOSED, []), # Note 3
    ],
    ConnectionStatus.SYN_RECEIVED: [
        Transition(Flags.ACK, ConnectionStatus.ESTABLISHED, []), #rcv ACK of SYN
        Transition(Events.CLOSE, next_state=ConnectionStatus.FIN_WAIT_1, [send(FLAGS.FIN)]),
        Transition(Flags.RST, next_state=ConnectionStatus.LISTEN, [is_passive]),
    ],
    ConnectionStatus.LISTEN: [
        Transition(Events.CLOSE, next_state=ConnectionStatus.CLOSED, [delete_tcb()]),
        Transition(Events.SEND, next_state=ConnectionStatus.SYN_SENT, [send(FLAGS.SYN)]),
        Transition(Flags.SYN, next_state=ConnectionStatus.SYN_RECEIVED, [send(FLAGS.SYN), send(FLAGS.ACK)]),
    ],
    ConnectionStatus.ESTABLISHED: [
        Transition(Events.CLOSE, next_state=ConnectionStatus.FIN_WAIT_1, [send(FLAGS.FIN)]),
        Transition(Flags.FIN, next_state=ConnectionStatus.CLOSE_WAIT, [send(FLAGS.ACK)]),
    ],

    ConnectionStatus.FIN_WAIT_1: [
        Transition(Flags.FIN, next_state=ConnectionStatus.CLOSING, [send(FLAGS.ACK)]),
        Transition(Flags.ACK, next_state=ConnectionStatus.FIN_WAIT_2, []), #rcv ACK of FIN
    ],
    ConnectionStatus.FIN_WAIT_2: [
        Transition(Flags.FIN, next_state=ConnectionStatus.TIME_WAIT, [send(FLAGS.ACK)]),
    ],
    ConnectionStatus.CLOSING: [
        Transition(Flags.ACK, next_state=ConnectionStatus.TIME_WAIT, []), #rcv ACK of FIN
    ],
    ConnectionStatus.TIME_WAIT: [
        Transition(Events.TIMEOUT, next_state=ConnectionStatus.CLOSED, [delete_tcb()]),
    ],

    ConnectionStatus.CLOSE_WAIT: [
        Transition(Events.CLOSE, next_state=ConnectionStatus.LAST_ACK, [send(FLAGS.FIN)]),
    ],
    ConnectionStatus.LAST_ACK: [
        Transition(Flags.ACK, next_state=ConnectionStatus.CLOSED, []), #rcv ACK of FIN
    ]
}

